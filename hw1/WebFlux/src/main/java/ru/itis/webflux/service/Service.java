package ru.itis.webflux.service;

import reactor.core.publisher.Flux;
import ru.itis.webflux.entity.Student;

public interface Service {
    Flux<Student> getStudents();
}
