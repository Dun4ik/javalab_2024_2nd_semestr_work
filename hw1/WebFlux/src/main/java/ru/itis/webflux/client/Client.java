package ru.itis.webflux.client;

import reactor.core.publisher.Flux;
import ru.itis.webflux.entity.Student;

public interface Client {
    Flux<Student> getStudents();
}
