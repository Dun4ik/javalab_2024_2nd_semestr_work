package ru.itis.webflux.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.itis.webflux.client.Client;
import ru.itis.webflux.entity.Student;

import java.util.List;

@Component
@AllArgsConstructor
public class StudentServiceImpl implements Service {

    private final List<Client> clients;

    @Override
    public Flux<Student> getStudents() {
        List<Flux<Student>> fluxes = clients.stream().map(this::getStudents).toList();
        return Flux.merge((fluxes));
    }

    private Flux<Student> getStudents(Client client) {
        return client.getStudents()
                .subscribeOn(Schedulers.boundedElastic());
    }

}
