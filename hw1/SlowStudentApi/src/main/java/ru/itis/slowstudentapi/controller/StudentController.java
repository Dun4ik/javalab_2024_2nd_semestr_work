package ru.itis.slowstudentapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.slowstudentapi.entity.Student;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @GetMapping()
    public List<Student> getAllStudents() throws InterruptedException {
        Thread.sleep(5000);
        return List.of(
                Student.builder()
                        .name("Иван Иванов")
                        .university("КФУ")
                        .build(),
                Student.builder()
                        .name("Пётр Петров")
                        .university("МГУ")
                        .build(),
                Student.builder()
                        .name("Максим Максимов")
                        .university("МФТИ")
                        .build()
        );
    }
}
