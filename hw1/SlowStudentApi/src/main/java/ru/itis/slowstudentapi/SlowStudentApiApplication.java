package ru.itis.slowstudentapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlowStudentApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SlowStudentApiApplication.class, args);
    }

}
