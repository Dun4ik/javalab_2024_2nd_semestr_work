package ru.itis.studentapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.studentapi.entity.Student;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @GetMapping()
    public List<Student> getAllStudents() {
        return List.of(
                Student.builder()
                        .name("Иван Иванов")
                        .university("КФУ")
                        .build(),
                Student.builder()
                        .name("Пётр Петров")
                        .university("МГУ")
                        .build(),
                Student.builder()
                        .name("Максим Максимов")
                        .university("МФТИ")
                        .build()
        );
    }
}
