package application

import (
	"context"
	"cv-service/internal/config"
	"cv-service/internal/database"
	"cv-service/internal/repository/mongo"
	"cv-service/internal/service"
	server "cv-service/internal/transport/grpc"
	"fmt"
	"log"
)

func Run(ctx context.Context) error {
	cfg, err := config.Parse()
	if err != nil {
		return fmt.Errorf("build config: %w", err)
	}

	mongoDatabase, err := database.SetupMongoDatabase(ctx, &cfg.Mongo)
	if err != nil {
		return fmt.Errorf("start mongo connection: %w", err)
	}

	CVRepository := mongo.NewCVRepository(mongoDatabase.Collection("cvs"))

	CVService := service.NewCVService(CVRepository)

	err = server.ServeGRPC(&cfg.GRPC, CVService)
	if err != nil {
		return fmt.Errorf("start gRPC server: %w", err)
	}

	log.Printf("gRPC started at %v\n", cfg.GRPC.Port)

	<-ctx.Done()
	log.Println("gRPC server shutting down")

	return nil
}
