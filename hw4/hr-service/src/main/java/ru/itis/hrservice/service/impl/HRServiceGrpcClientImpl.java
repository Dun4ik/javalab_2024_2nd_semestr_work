package ru.itis.hrservice.service.impl;

import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.itis.hrservice.dto.CV;
import ru.itis.hrservice.dto.CvJobPair;
import ru.itis.hrservice.dto.Job;
import ru.itis.hrservice.grpc.pb.cv.CVServiceGrpc;
import ru.itis.hrservice.grpc.pb.job.GetAllRequest;
import ru.itis.hrservice.grpc.pb.job.GetAllResponse;
import ru.itis.hrservice.grpc.pb.job.JobServiceGrpc;
import ru.itis.hrservice.service.HRService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HRServiceGrpcClientImpl implements HRService {

    @GrpcClient("job-service")
    private JobServiceGrpc.JobServiceBlockingStub jobService;

    @GrpcClient("cv-service")
    private CVServiceGrpc.CVServiceBlockingStub cvService;

    @Override
    public List<CvJobPair> getPairs() {
        List<CvJobPair> pairs = new ArrayList<>();

        GetAllResponse jobsResponse = jobService.getAll(GetAllRequest.newBuilder().build());

        List<Job> jobs = jobsResponse.getJobsList().stream().map(j -> Job.builder()
                .name(j.getName())
                .experience(j.getExperience())
                .skills(j.getSkillsList())
                .description(j.getDescription())
                .company(j.getCompany())
                .build()).toList();

        ru.itis.hrservice.grpc.pb.cv.GetAllResponse cvsResponse = cvService.getAll(ru.itis.hrservice.grpc.pb.cv.GetAllRequest.newBuilder().build());

        List<CV> cvs = cvsResponse.getCvsList().stream().map(c -> CV.builder()
                .firstName(c.getFirstName())
                .lastName(c.getLastName())
                .skills(c.getSkillsList())
                .experience(c.getExperience())
                .profession(c.getProfession())
                .build()).toList();

        for (Job job : jobs) {
            for (CV cv : cvs) {
                boolean enoughExperience = cv.getExperience() >= job.getExperience();
                boolean enoughSkills = true;
                if (enoughExperience) {
                    for (String skill : job.getSkills()) {
                        if (!cv.getSkills().contains(skill)) {
                            enoughSkills = false;
                            break;
                        }
                    }
                    if (enoughSkills) {
                        pairs.add(CvJobPair.builder()
                                .cv(cv)
                                .job(job)
                                .build());
                    }
                }
            }
        }

        return pairs;
    }
}
